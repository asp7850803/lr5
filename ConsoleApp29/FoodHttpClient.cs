﻿using System.Net;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using static System.Runtime.InteropServices.JavaScript.JSType;

public class FoodHttpClient { 
private readonly HttpClient _HttpClient;
private readonly string _ApiKey;

public FoodHttpClient(string apiKey)
    {
        _HttpClient = new HttpClient { BaseAddress=new Uri("https://api.spoonacular.com/") } ;
        _ApiKey = apiKey;
    }

    public async Task<HttpClientResponse<SummaryRecipeResponse>> Get(int id)
    {
        try
        {
            var response = await _HttpClient.GetAsync($"recipes/{id}/summary?apiKey={_ApiKey}");
            response.EnsureSuccessStatusCode();
            var responseData = await response.Content.ReadAsStringAsync();
            var data = JsonSerializer.Deserialize<SummaryRecipeResponse>(responseData);
            return new HttpClientResponse<SummaryRecipeResponse>
            {
                Data = new List<SummaryRecipeResponse> { data },
                StatusCode = response.StatusCode,
                Message="Done"
            };
        }catch (HttpRequestException ex) {
            return new HttpClientResponse<SummaryRecipeResponse>
            {
                Message = ex.Message,
                StatusCode = HttpStatusCode.InternalServerError,
                Data = new List<SummaryRecipeResponse>()
            };
         }
    }

    public async Task<HttpClientResponse<ClassifyGroceryProductResponse>> Post(string name)
    {
        try
        {
            var jsonData = new
            {
                plu_code = "",
                title = $"{name}",
                upc = ""
            };
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(jsonData);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _HttpClient.PostAsync($"food/products/classify?apiKey={_ApiKey}", content);
            response.EnsureSuccessStatusCode();
            var responseData = await response.Content.ReadAsStringAsync();
            var data = JsonSerializer.Deserialize<ClassifyGroceryProductResponse>(responseData);
            return new HttpClientResponse<ClassifyGroceryProductResponse>
            {
                Data = new List<ClassifyGroceryProductResponse> { data },
                StatusCode = response.StatusCode,
                Message = "Done"
            };
        }
        catch (HttpRequestException ex)
        {
            return new HttpClientResponse<ClassifyGroceryProductResponse>
            {
                Message = ex.Message,
                StatusCode = HttpStatusCode.InternalServerError,
                Data = new List<ClassifyGroceryProductResponse>()
            };
        }
    }
}

    



public class HttpClientResponse<T>
{
    public string Message { get; set; }
    public List<T> Data { get; set; }
    public HttpStatusCode StatusCode { get; set; }
}

public class SummaryRecipeResponse
{
    [JsonPropertyName("id")]
    public int Id { get; set; }
    [JsonPropertyName("title")]
    public string Title { get; set; }
    [JsonPropertyName("summary")]
    public string Summary { get; set; }
}

public class ClassifyGroceryProductResponse
{
    [JsonPropertyName("matched")]
    public string Matched { get; set; }
    [JsonPropertyName("breadcrumbs")]
    public string[] Breadcrumbs { get; set; }
    [JsonPropertyName("category")]
    public string Category { get; set; }
    [JsonPropertyName("usdaCode")]
    public int UsdaCode { get; set; }
    [JsonPropertyName("image")]
    public string Image { get; set; }
    [JsonPropertyName("cleanTitle")]
    public string CleanTitle { get; set; }
    [JsonPropertyName("ingredientId")]
    public int IngredientId { get; set; }
}