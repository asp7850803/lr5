﻿using Newtonsoft.Json;
using System.Text.Json;

string json = File.ReadAllText("apiKey.json");
dynamic jsonObject = JsonConvert.DeserializeObject(json);
string apiKey = jsonObject.key;
FoodHttpClient foodHttpClient = new FoodHttpClient(apiKey);
var data = await foodHttpClient.Get(109);
Console.WriteLine($"Message: {data.Message}");
Console.WriteLine($"Status code: {data.StatusCode}");
Console.WriteLine($"Title: {data.Data[0].Title}");
Console.WriteLine($"Summary: {data.Data[0].Summary}");
var data2 = await foodHttpClient.Post("Kroger Vitamin A & D Reduced Fat 2% Milk");
Console.WriteLine($"Message 2: {data2.Message}");
Console.WriteLine($"Status code 2: {data2.StatusCode}");
Console.WriteLine($"Matched by:{data2.Data[0].Matched}");
foreach (var item in data2.Data[0].Breadcrumbs)
{
    Console.WriteLine($"Breadcumb:{item}");
}
Console.WriteLine($"Category:{data2.Data[0].Category}");
Console.WriteLine($"Clean title:{data2.Data[0].CleanTitle}");

